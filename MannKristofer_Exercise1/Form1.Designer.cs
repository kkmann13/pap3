﻿namespace MannKristofer_Exercise1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.deleteHaveButton = new System.Windows.Forms.Button();
            this.moveToNeedButton = new System.Windows.Forms.Button();
            this.haveListBox = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.needButton = new System.Windows.Forms.Button();
            this.haveButton = new System.Windows.Forms.Button();
            this.itemsTextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.needListBox = new System.Windows.Forms.ListBox();
            this.moveToHaveButton = new System.Windows.Forms.Button();
            this.deleteNeedButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.deleteHaveButton);
            this.groupBox1.Controls.Add(this.moveToNeedButton);
            this.groupBox1.Controls.Add(this.haveListBox);
            this.groupBox1.Location = new System.Drawing.Point(27, 54);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(266, 468);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Have List";
            // 
            // deleteHaveButton
            // 
            this.deleteHaveButton.Location = new System.Drawing.Point(40, 417);
            this.deleteHaveButton.Name = "deleteHaveButton";
            this.deleteHaveButton.Size = new System.Drawing.Size(169, 45);
            this.deleteHaveButton.TabIndex = 2;
            this.deleteHaveButton.Text = "Delete Item";
            this.deleteHaveButton.UseVisualStyleBackColor = true;
            this.deleteHaveButton.Click += new System.EventHandler(this.deleteHaveButton_Click);
            // 
            // moveToNeedButton
            // 
            this.moveToNeedButton.Location = new System.Drawing.Point(40, 352);
            this.moveToNeedButton.Name = "moveToNeedButton";
            this.moveToNeedButton.Size = new System.Drawing.Size(169, 45);
            this.moveToNeedButton.TabIndex = 1;
            this.moveToNeedButton.Text = "Move to Need";
            this.moveToNeedButton.UseVisualStyleBackColor = true;
            this.moveToNeedButton.Click += new System.EventHandler(this.moveToNeedButton_Click);
            // 
            // haveListBox
            // 
            this.haveListBox.FormattingEnabled = true;
            this.haveListBox.ItemHeight = 25;
            this.haveListBox.Location = new System.Drawing.Point(7, 31);
            this.haveListBox.Name = "haveListBox";
            this.haveListBox.Size = new System.Drawing.Size(253, 304);
            this.haveListBox.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(884, 42);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 38);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(269, 38);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(269, 38);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.needButton);
            this.groupBox2.Controls.Add(this.haveButton);
            this.groupBox2.Controls.Add(this.itemsTextBox);
            this.groupBox2.Location = new System.Drawing.Point(299, 156);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(296, 174);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Enter items into a list";
            // 
            // needButton
            // 
            this.needButton.Location = new System.Drawing.Point(169, 100);
            this.needButton.Name = "needButton";
            this.needButton.Size = new System.Drawing.Size(121, 45);
            this.needButton.TabIndex = 3;
            this.needButton.Text = "Need List";
            this.needButton.UseVisualStyleBackColor = true;
            this.needButton.Click += new System.EventHandler(this.needButton_Click);
            // 
            // haveButton
            // 
            this.haveButton.Location = new System.Drawing.Point(6, 100);
            this.haveButton.Name = "haveButton";
            this.haveButton.Size = new System.Drawing.Size(121, 45);
            this.haveButton.TabIndex = 2;
            this.haveButton.Text = "Have List";
            this.haveButton.UseVisualStyleBackColor = true;
            this.haveButton.Click += new System.EventHandler(this.haveButton_Click);
            // 
            // itemsTextBox
            // 
            this.itemsTextBox.Location = new System.Drawing.Point(56, 52);
            this.itemsTextBox.Name = "itemsTextBox";
            this.itemsTextBox.Size = new System.Drawing.Size(170, 31);
            this.itemsTextBox.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.deleteNeedButton);
            this.groupBox3.Controls.Add(this.moveToHaveButton);
            this.groupBox3.Controls.Add(this.needListBox);
            this.groupBox3.Location = new System.Drawing.Point(601, 54);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(266, 468);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Need List";
            // 
            // needListBox
            // 
            this.needListBox.FormattingEnabled = true;
            this.needListBox.ItemHeight = 25;
            this.needListBox.Location = new System.Drawing.Point(7, 31);
            this.needListBox.Name = "needListBox";
            this.needListBox.Size = new System.Drawing.Size(253, 304);
            this.needListBox.TabIndex = 1;
            // 
            // moveToHaveButton
            // 
            this.moveToHaveButton.Location = new System.Drawing.Point(53, 352);
            this.moveToHaveButton.Name = "moveToHaveButton";
            this.moveToHaveButton.Size = new System.Drawing.Size(169, 45);
            this.moveToHaveButton.TabIndex = 2;
            this.moveToHaveButton.Text = "Move to Have";
            this.moveToHaveButton.UseVisualStyleBackColor = true;
            this.moveToHaveButton.Click += new System.EventHandler(this.moveToHaveButton_Click);
            // 
            // deleteNeedButton
            // 
            this.deleteNeedButton.Location = new System.Drawing.Point(53, 417);
            this.deleteNeedButton.Name = "deleteNeedButton";
            this.deleteNeedButton.Size = new System.Drawing.Size(169, 45);
            this.deleteNeedButton.TabIndex = 3;
            this.deleteNeedButton.Text = "Delete Item";
            this.deleteNeedButton.UseVisualStyleBackColor = true;
            this.deleteNeedButton.Click += new System.EventHandler(this.deleteNeedButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 547);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Party List";
            this.groupBox1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button deleteHaveButton;
        private System.Windows.Forms.Button moveToNeedButton;
        private System.Windows.Forms.ListBox haveListBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button needButton;
        private System.Windows.Forms.Button haveButton;
        private System.Windows.Forms.TextBox itemsTextBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListBox needListBox;
        private System.Windows.Forms.Button deleteNeedButton;
        private System.Windows.Forms.Button moveToHaveButton;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MannKristofer_Exercise1
{
    public partial class Form1 : Form
    {
        //get set Items.Item
        public Items Data
        {
            get
            {
                Items i = new Items();
                i.Item = itemsTextBox.Text;
                return i;
            }

            set
            {
                itemsTextBox.Text = value.Item;
            }
        } 

        public Form1()
        {
            InitializeComponent();
        }

        //method for removing an item from the have list
        public void RemoveHaveItem()
        {
            for (int i = haveListBox.SelectedIndices.Count -1; i >=0; i--)
            {
                haveListBox.Items.RemoveAt(haveListBox.SelectedIndices[i]);
            }
        }

        //method for removing an item from the need list
        public void RemoveNeedItem()
        {
            for (int i = needListBox.SelectedIndices.Count -1; i >=0; i--)
            {
                needListBox.Items.RemoveAt(needListBox.SelectedIndices[i]);
            }
        }

        //exits the application
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //adds the item to the have list and clears the textbox
        private void haveButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(Data.Item))
            {
                haveListBox.Items.Add(Data);
            }           
            Data = new Items();
        }

        //adds the item to the need list and clears the textbox
        private void needButton_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(Data.Item))
            {
                needListBox.Items.Add(Data);
            }
            Data = new Items();
        }

        //deletes the selected have item using the remove method
        private void deleteHaveButton_Click(object sender, EventArgs e)
        {
            RemoveHaveItem();
        }

        //deletes the selected need item using the remove method
        private void deleteNeedButton_Click(object sender, EventArgs e)
        {
            RemoveNeedItem();
        }

        //moves an item from have to need and removes it from have list
        private void moveToNeedButton_Click(object sender, EventArgs e)
        {
            //make sure it does not throw an exception
            if (haveListBox.SelectedItem!=null)
            {
                needListBox.Items.Add(haveListBox.SelectedItem);
            }
            
            RemoveHaveItem();
        }

        //moves an item from need to have and removes it from need list
        private void moveToHaveButton_Click(object sender, EventArgs e)
        {
            //prevents exception
            if (needListBox.SelectedItem != null)
            {
                haveListBox.Items.Add(needListBox.SelectedItem);
            }

            RemoveNeedItem();
        }

        //method for saving items from both list to local computer
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(sfd.FileName + @".txt"))
                {
                    //save items from have list
                    foreach (Items i in haveListBox.Items)
                    {
                        sw.WriteLine(i.SaveHave());
                    }

                    //save items from need list
                    foreach (Items i in needListBox.Items)
                    {
                        sw.WriteLine(i.SaveNeed());
                    }
                }
            }
        }
    }
}

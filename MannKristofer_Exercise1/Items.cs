﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MannKristofer_Exercise1
{
    public class Items
    {
        //default constructor
        public Items() { }

        //declare variables
        string item;
        string list;

        //get set item
        public string Item
        {
            get { return item; }
            set { item = value; }
        }

        //get set list
        public string List
        {
            get { return list; }
            set { list = value; }
        }

        //override tostring method
        public override string ToString()
        {
            return Item;
        }

        //method for storing have item and location
        public string SaveHave()
        {
            string have;
            List = "Have";
            have = Item + "," + List;

            return have;
        }

        //method for storing need item and location
        public string SaveNeed()
        {
            string need;
            List = "Need";
            need = Item + "," + List;

            return need;
        }


    }
}
